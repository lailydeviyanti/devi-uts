<?php

namespace App\Http\Controllers;

use App\Models\Tabel;
use Illuminate\Http\Request;

class TabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pedagang = Tabel::all();
        return view('tabel-pedagang', compact('pedagang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambah-pedagang');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Tabel::create([
            'nama' => $request->nama,
            'posisi' => $request->posisi,
            'status' => $request->status,
            'bergabung' => $request->bergabung,
        ]);

        return redirect('pedagang');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tabel  $tabel
     * @return \Illuminate\Http\Response
     */
    public function show(Tabel $id)
    {
        $psatu = Tabel::find($id);
        return view('satu-pedagang', compact('psatu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tabel  $tabel
     * @return \Illuminate\Http\Response
     */
    public function edit(Tabel $tabel, $id)
    {
        $pedit = Tabel::findorfail($id);
        return view('edit-pedagang', compact('pedit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tabel  $tabel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pedit = Tabel::findorfail($id);
        $pedit->update($request->all());

        return redirect('pedagang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tabel  $tabel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tabel $tabel, $id)
    {
        $pedit = Tabel::findorfail($id);
        $pedit->delete();

        return back();
    }
}
