<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tabel;

class TabelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tabel::create([
            'nama' => 'Devi',
            'posisi' => 'Owner',
            'status' => 'Aktif',
            'bergabung' => '22-19-2015',
        ]);
    }
}
