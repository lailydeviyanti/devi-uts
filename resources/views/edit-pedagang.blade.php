
    

@extends('index')

@section('konten')
    <main class="main-content position-relative max-height-vh-100 h-100 mt-1 border-radius-lg ">
    <!-- Navbar -->
    <nav class="navbar navbar-main navbar-expand-lg px-0 mx-4 shadow-none border-radius-xl" id="navbarBlur" navbar-scroll="true">
      <div class="container-fluid py-1 px-3">
        <div class="buat">
            <h2>Edit Data Pedagang</h2>
    <form action="{{ url('update-pedagang', $pedit->id) }}" method="post">
    {{ csrf_field() }}
        <div class="form-grup">
            <p>Nama : </p>
            <input type="text" id="nama" name="nama" placeholder="Nama" value="{{ $pedit->nama }}">                        
        </div>
        <div class="form-grup">
            <br>
            <p>Posisi : </p>
            <input type="text" id="posisi" name="posisi" placeholder="Posisi" value="{{ $pedit->posisi }}">
        </div>
        <div class="form-grup">
            <br>
            <p>Status : </p>
            <input type="text" id="status" name="status" placeholder="Status" value="{{ $pedit->status }}">
        </div>
        <div class="form-grup">
            <br>
            <p>Tanggal Bergabung : </p>
            <input type="text" id="bergabung" name="bergabung" placeholder="Status" value="{{ $pedit->bergabung }}">
        </div>
        <div class="form-grup">
                <br>
                <button type="submit" class="button-succes">Simpan Edit</button>
        </div>
        </div>
    </form>
        </div>
      </div>
    </nav>
  </main>
@endsection