<?php

use App\Http\Controllers\TabelController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('page-index');
});
Route::get('/barang', function () {
    return view('tabel-barang');
});
// Route::get('/pedagang', function () {
//     return view('tabel-pedagang');
// });
Route::get('/test', function () {
    return view('tabel-test');
});

Route::get('pedagang',[TabelController::class, 'index']);
Route::get('pedagang/{id}', [TabelController::class, 'show']);
Route::get('tambah-pedagang', [TabelController::class, 'create']);
Route::post('simpan-pedagang', [TabelController::class, 'store']);
Route::get('edit-pedagang/{id}', [TabelController::class, 'edit']);
Route::post('update-pedagang/{id}', [TabelController::class, 'update']);
Route::get('delete-pedagang/{id}', [TabelController::class, 'destroy']);
